module.exports = (sequelize, Sequelize) => {
    const List = sequelize.define("list", {
      listId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      listName: {
        type: Sequelize.STRING,
      },
      boardId: { 
        type: Sequelize.INTEGER,
      },
    });
  
    List.associate = (models) => {
      List.hasMany(models.Card, {
        foreignKey: "listId",
        as: "card",
      });
  
      List.belongsTo(models.Board, {
        foreignKey: "boardId",
        onDelete: "CASCADE",
      });
    };
  
    return List;
  };
  