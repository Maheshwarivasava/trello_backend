module.exports = (sequelize, Sequelize) => {
  const Card = sequelize.define("card", {
    cardId: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    cardName: {
      type: Sequelize.STRING,
    },
    listId: {
      type: Sequelize.INTEGER,
    },
  });

  Card.associate = (models) => {
    Card.hasMany(models.Checklist, {
      foreignKey: "cardId",
      as: "checklist",
    });

    Card.belongsTo(models.List, {
      foreignKey: "listId",
      onDelete: "CASCADE",
    });
  };
  return Card;
};
