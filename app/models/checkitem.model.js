module.exports = (sequelize, Sequelize) => {
  const CheckItem = sequelize.define("checkitem", {
    checkitemId: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    checkitemName: {
      type: Sequelize.STRING,
    },
    completed: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    checklistId: {
      type: Sequelize.INTEGER,
    },
  });
  
  CheckItem.associate = (models) => {
    CheckItem.belongsTo(models.Checklist, {
      foreignKey: "checklistId",
      onDelete: "CASCADE",
    });
  };

  return CheckItem;
};
