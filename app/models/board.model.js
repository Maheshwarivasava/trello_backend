module.exports = (sequelize, Sequelize) => {
  const Board = sequelize.define("board", {
    boardId: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true, 
    },
    boardName: {
      type: Sequelize.STRING,
    },
    userId: {
      type: Sequelize.INTEGER,
    },
  });

  Board.associate = (models) => {
    Board.hasMany(models.List, {
      foreignKey: "boardId",
      as: "list",
    });

    Board.belongsTo(models.User, {
      foreignKey: "userId",
      onDelete: "CASCADE",
    });
  };

  return Board;
};

//SET NULL: This option sets the foreign key in the child records to NULL when the associated record in the parent table is deleted.

//SET DEFAULT: This option sets the foreign key in the child records to its default value when the associated record in the parent table is deleted.

//RESTRICT: This option prevents deletion of a record in the parent table if there are associated records in the child table. It essentially enforces referential integrity by restricting the deletion operation.

//NO ACTION: This option also prevents deletion of a record in the parent table if there are associated records in the child table. It behaves similarly to RESTRICT but might have different behavior depending on the database system.

//CASCADE: Automatically deletes all corresponding rows in the child table when the referenced row in the parent table is deleted.