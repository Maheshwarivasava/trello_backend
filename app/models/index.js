const Sequelize = require("sequelize");
const config = require("../config/db.config.js");

const sequelize = new Sequelize(
  config.DB,
  config.USER,
  config.PASSWORD,
  {
    host: config.HOST,
    dialect: config.dialect,
    pool: {
      max: config.pool.max,
      min: config.pool.min,
      acquire: config.pool.acquire,
      idle: config.pool.idle
    }
  }
);

const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.User = require("./user.model.js")(sequelize, Sequelize);
db.Board = require("./board.model.js")(sequelize, Sequelize);
db.List = require("./list.model.js")(sequelize, Sequelize);
db.Card = require("./card.model.js")(sequelize, Sequelize);
db.Checklist = require("./checklist.model.js")(sequelize, Sequelize);
db.CheckItem = require("./checkitem.model.js")(sequelize, Sequelize);
db.Tag = require("./tag.model.js")(sequelize, Sequelize);


db.Card.belongsToMany(db.Tag, {through: "cardtotags", as: "tags", foreignKey: "cardId"});
db.Tag.belongsToMany(db.Card, {through: 'cardtotags', as: "cards", foreignKey: "tagId"});

module.exports = db;
