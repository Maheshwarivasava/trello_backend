module.exports = (sequelize, Sequelize) => {
    const Checklist = sequelize.define("checklist", {
      checklistId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      checklistName: {
        type: Sequelize.STRING,
      },
      cardId: { 
        type: Sequelize.INTEGER,
      },
    });
  
    Checklist.associate = (models) => {
      Checklist.hasMany(models.CheckItem, {
        foreignKey: "checklistId",
        as: "checkitem",
      });
  
      Checklist.belongsTo(models.Card, {
        foreignKey: "cardId",
        onDelete: "CASCADE",
      });
    };
  
    return Checklist;
  };
  