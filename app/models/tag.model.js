module.exports = (sequelize, Sequelize) => {
  const Tag = sequelize.define("tag", {
      tagId: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
      },
      tagName: {
          type: Sequelize.STRING,
          allowNull: false,
      },
  });

  return Tag;
};
