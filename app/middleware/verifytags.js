
exports.verifyTags = (req, res, next) => {
    const predefinedTags = ["red", "green", "yellow", "blue", "black"];
    if (req.body.tags) {
      for (let i = 0; i < req.body.tags.length; i++) {
        if (!predefinedTags.includes(req.body.tags[i])) {
          res.status(400).send({
            message: "Failed! Tag does not exist: " + req.body.tags[i],
          });
          return;
        }
      }
    }
  
    next();
  };
  