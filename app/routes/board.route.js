var router = require("express").Router();

const boardController = require("../controllers/board.controller");

router.post("/:userId/boards", boardController.create);
router.get("/:userId/boards", boardController.findAllByUserId);
router.get("/boards/:id", boardController.findOne);
router.delete("/boards/:id", boardController.delete);

const listController = require("../controllers/list.controller");

router.post("/boards/:boardId/lists", listController.create);
router.get("/boards/:boardId/lists", listController.findAllbyBoardId);
router.get("/lists/:id", listController.findOne);
router.delete("/lists/:id", listController.delete);

const cardController = require("../controllers/card.controller");

router.post("/lists/:listId/cards", cardController.create);
router.get("/lists/:listId/cards", cardController.findAllbyListId);
router.get("/cards/:id", cardController.findOne);
router.delete("/cards/:id", cardController.delete);

const tagController = require("../controllers/tag.controller");
router.get("/tags/:tagName", tagController.findByTagName);//tag

const checklistController = require("../controllers/checklist.controller");

router.post("/cards/:cardId/checklists", checklistController.create);
router.get("/cards/:cardId/checklists", checklistController.findAllbyCardId);
router.get("/checklist/:id", checklistController.findOne);
router.delete("/checklist/:id", checklistController.delete);

const checkitemController = require("../controllers/checkitem.controller");

router.post("/checklists/:checklistId/checkitems", checkitemController.create);
router.get( "/checklists/:checklistId/checkitems",checkitemController.findAllbyCheckListId);
router.get("/checkitem/:id", checkitemController.findOne);
router.delete("/checkitem/:id", checkitemController.delete);

module.exports = router;
