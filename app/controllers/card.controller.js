const db = require("../models");
const Card = db.Card;
const Tag = db.Tag;

const fixedTags = ["red", "green", "yellow", "blue", "black"];

const Op = db.Sequelize.Op;

exports.create = async (req, res) => {
  try {
    if (!req.body.cardName || req.body.cardName.trim() === "") {
      return res.status(400).send({
        message: "Card name cannot be empty!",
      });
    }

    const cardData = await Card.create({
      cardName: req.body.cardName,
      listId: req.params.listId,
    });

    let cardTags = [];
    if (req.body.tags) {
      const tagData = await Tag.findAll({
        where: {
          tagName: {
            [Op.or]: req.body.tags,
          },
        },
      });
      cardTags = tagData.map((tag) => tag.tagId);
    }

    await cardData.setTags(cardTags);
    const storedTags = [];
    const tags = await cardData.getTags();
    for (let i = 0; i < tags.length; i++) {
      storedTags.push( tags[i].tagName.toUpperCase());
    }

    // res.send(cardData);
    res.status(200).send({
      cardId: cardData.cardId,
      cardName: cardData.cardName,
      listId: cardData.listId,
      tags: storedTags,
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({
      message: err.message || "Some error occurred while creating the Card.",
    });
  }
};

exports.findAllbyListId = async (req, res) => {
  try {
    const listId = req.params.listId;
    const data = await Card.findAll({
      where: {
        listId: listId,
      },
    });
    res.send(data);
  } catch (err) {
    console.error(err);
    res.status(500).send({
      message: err.message || "Some error occurred while retrieving cards.",
    });
  }
};

exports.findOne = async (req, res) => {
  try {
    const id = req.params.id;
    const data = await Card.findByPk(id);
    if (data) {
      res.send(data);
    } else {
      res.status(404).send({
        message: `Cannot find Card with id=${id}.`,
      });
    }
  } catch (err) {
    console.error(err);
    res.status(500).send({
      message: "Error retrieving Card with id=" + id,
    });
  }
};

exports.delete = async (req, res) => {
  try {
    const id = req.params.id;
    const num = await Card.destroy({
      where: { cardId: id },
    });
    if (num == 1) {
      res.send({
        message: "Card was deleted successfully!",
      });
    } else {
      res.send({
        message: `Cannot delete Card with id=${id}. Maybe Card was not found!`,
      });
    }
  } catch (err) {
    console.error(err);
    res.status(500).send({
      message: "Could not delete Card with id=" + id,
    });
  }
};


// exports.findByTagName = async (req, res) => {
//   try {
//     const tagName = req.params.tagName;

//     // Find all cards associated with the given tag name
//     const cardswithtags = await Card.findAll({
//       include: [
//         {
//           model: Tag,
//           as: "tags",
//           attributes: ["tagId", "tagName"],
//           through: { attributes: [] },
//           where: {
//             tagName: tagName,
//           },
//         },
//       ],
//     });

//     console.log("hello",cardswithtags);
//     // Send the retrieved cards as the response
//     res.send(cardswithtags);
//   } catch (err) {
//     console.error(err);
//     res.status(500).send({
//       message: err.message || `Some error occurred while retrieving cards by tag name "${tagName}".`,
//     });
//   }
// };
