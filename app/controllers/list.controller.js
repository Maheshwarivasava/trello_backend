const db = require("../models");
const List = db.List;

// Create and Save a new List
exports.create = async (req, res) => {
  try {
    // Validate request
    if (!req.body.listName) {
      return res.status(400).send({
        message: "List name cannot be empty!"
      });
    }

    // Save List in the database
    const data = await List.create({
      listName: req.body.listName,
      boardId: req.params.boardId
    });
    res.send(data);
  } catch (err) {
    res.status(500).send({
      message: err.message || "Some error occurred while creating the List."
    });
  }
};


// Retrieve all Lists by BoardId from the database.
exports.findAllbyBoardId = async (req, res) => {
  try {
    const boardId = req.params.boardId;
    const data = await List.findAll({
      where: {
        boardId: boardId
      }
    });
    res.send(data);
  } catch (err) {
    res.status(500).send({
      message: err.message || "Some error occurred while retrieving lists."
    });
  }
};

// Find a single List with an id
exports.findOne = async (req, res) => {
  try {
    const id = req.params.id;
    const data = await List.findByPk(id);
    if (data) {
      res.send(data);
    } else {
      res.status(404).send({
        message: `Cannot find List with id=${id}.`
      });
    }
  } catch (err) {
    res.status(500).send({
      message: "Error retrieving List with id=" + id
    });
  }
};

// Delete a List with the specified id in the request
exports.delete = async (req, res) => {
  try {
    const id = req.params.id;
    const num = await List.destroy({
      where: { listId: id }
    });
    if (num == 1) {
      res.send({
        message: "List was deleted successfully!"
      });
    } else {
      res.send({
        message: `Cannot delete List with id=${id}. Maybe List was not found!`
      });
    }
  } catch (err) {
    res.status(500).send({
      message: "Could not delete List with id=" + id
    });
  }
};
