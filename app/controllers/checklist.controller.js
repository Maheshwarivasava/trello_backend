const db = require("../models");
const Checklist = db.Checklist;

// Create and Save a new Checklist
exports.create = async (req, res) => {
  try {
    if (!req.body.checklistName) {
      return res.status(400).send({
        message: "Checklist name cannot be empty!"
      });
    }

    const data = await Checklist.create({
      checklistName: req.body.checklistName,
      cardId: req.params.cardId
    });
    res.send(data);
  } catch (err) {
    res.status(500).send({
      message: err.message || "Some error occurred while creating the Checklist."
    });
  }
};

// Retrieve all Checklists from the database.
exports.findAllbyCardId = async (req, res) => {
  try {
    const cardId = req.params.cardId;
    const data = await Checklist.findAll({
      where: {
        cardId: cardId
      }
    });
    res.send(data);
  } catch (err) {
    res.status(500).send({
      message: err.message || "Some error occurred while retrieving checklists."
    });
  }
};

// Find a single Checklist with an id
exports.findOne = async (req, res) => {
  try {
    const id = req.params.id;
    const data = await Checklist.findByPk(id);
    if (data) {
      res.send(data);
    } else {
      res.status(404).send({
        message: `Cannot find Checklist with id=${id}.`
      });
    }
  } catch (err) {
    res.status(500).send({
      message: "Error retrieving Checklist with id=" + id
    });
  }
};

// Delete a Checklist with the specified id in the request
exports.delete = async (req, res) => {
  try {
    const id = req.params.id;
    const num = await Checklist.destroy({
      where: { checklistId: id }
    });
    if (num == 1) {
      res.send({
        message: "Checklist was deleted successfully!"
      });
    } else {
      res.send({
        message: `Cannot delete Checklist with id=${id}. Maybe Checklist was not found!`
      });
    }
  } catch (err) {
    res.status(500).send({
      message: "Could not delete Checklist with id=" + id
    });
  }
};
