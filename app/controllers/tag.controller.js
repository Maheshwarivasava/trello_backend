const db = require("../models");
const Card = db.Card;
const Tag = db.Tag;


const Op = db.Sequelize.Op;

exports.findByTagName = async (req, res) => {
  try {
    const tagName = req.params.tagName;

    // Find all cards associated with the given tag name
    const cardswithtags = await Card.findAll({
      include: [
        {
          model: Tag,
          as: "tags",
          attributes: ["tagId", "tagName"],
          through: { attributes: [] },
          where: {
            tagName: tagName,
          },
        },
      ],
    });

    console.log("hello",cardswithtags);
    // Send the retrieved cards as the response
    res.send(cardswithtags);
  } catch (err) {
    console.error(err);
    res.status(500).send({
      message: err.message || `Some error occurred while retrieving cards by tag name "${tagName}".`,
    });
  }
};
