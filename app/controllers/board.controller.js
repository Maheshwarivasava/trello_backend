const db = require("../models");
const Board = db.Board;

exports.create = async (req, res) => {
  // console.log('rgr');
  try {
    if (!req.body.boardName) {
      return res.status(400).send({
        message: "Board name cannot be empty!"
      });
    }
    const data = await Board.create({
      boardName: req.body.boardName,
      userId :req.params.userId,
    });
    res.send(data);
  } catch (err) {
    res.status(500).send({
      message: err.message || "Some error occurred while creating the Board."
    });
  }
};

exports.findAllByUserId = async (req, res) => {
  try {
    const userId = req.params.userId;
    const data = await Board.findAll({
      where: {
        userId: userId
      }
    });
    res.send(data);
  } catch (err) {
    res.status(500).send({
      message: err.message || "Some error occurred while retrieving boards."
    });
  }
};

exports.findOne = async (req, res) => {
  try {
    const id = req.params.id;
    const data = await Board.findByPk(id);
    if (data) {
      res.send(data);
    } else {
      res.status(404).send({
        message: `Cannot find Board with id=${id}.`
      });
    }
  } catch (err) {
    res.status(500).send({
      message: "Error retrieving Board with id=" + id
    });
  }
};

exports.delete = async (req, res) => {
  try {
    const id = req.params.id;
    const num = await Board.destroy({
      where: { boardId: id }
    });
    if (num == 1) {
      res.send({
        message: "Board was deleted successfully!"
      });
    } else {
      res.send({
        message: `Cannot delete Board with id=${id}. Maybe Board was not found!`
      });
    }
  } catch (err) {
    const id = req.params.id;
    res.status(500).send({
      message: "Could not delete Board with id=" + id
    });
  }
};
