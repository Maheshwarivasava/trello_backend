const db = require("../models");
const Checkitem = db.CheckItem;

// Create and Save a new Checkitem
exports.create = async (req, res) => {
  try {
    if (!req.body.checkitemName) {
      return res.status(400).send({
        message: "Checkitem name cannot be empty!",
      });
    }

    const data = await Checkitem.create({
      checkitemName: req.body.checkitemName,
      checklistId: req.params.checklistId,
      completed: req.body.completed || false,
    });
    res.send(data);
  } catch (err) {
    res.status(500).send({
      message:
        err.message || "Some error occurred while creating the Checkitem.",
    });
  }
};

// Retrieve all Checkitems from the database.

exports.findAllbyCheckListId = async (req, res) => {
  try {
    const checklistId = req.params.checklistId;
    const data = await Checkitem.findAll({
      where: {
        checklistId: checklistId,
      },
    });
    res.send(data);
  } catch (err) {
    res.status(500).send({
      message:
        err.message || "Some error occurred while retrieving checklists.",
    });
  }
};

// Find a single Checkitem with an id
exports.findOne = async (req, res) => {
  try {
    const id = req.params.id;
    const data = await Checkitem.findByPk(id);
    if (data) {
      res.send(data);
    } else {
      res.status(404).send({
        message: `Cannot find Checkitem with id=${id}.`,
      });
    }
  } catch (err) {
    res.status(500).send({
      message: "Error retrieving Checkitem with id=" + id,
    });
  }
};

// Delete a Checkitem with the specified id in the request
exports.delete = async (req, res) => {
  try {
    const id = req.params.id;
    const num = await Checkitem.destroy({
      where: { checkitemId: id },
    });
    if (num == 1) {
      res.send({
        message: "Checkitem was deleted successfully!",
      });
    } else {
      res.send({
        message: `Cannot delete Checkitem with id=${id}. Maybe Checkitem was not found!`,
      });
    }
  } catch (err) {
    res.status(500).send({
      message: "Could not delete Checkitem with id=" + id,
    });
  }
};
