const express = require("express");
const cors = require("cors");
const db = require("./app/models");

const app = express();
const PORT = process.env.PORT || 8080;

var corsOptions = {
  origin: "http://localhost:8081",
};

app.use(cors(corsOptions));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const authJwt= require("./app/middleware/authJwt")

app.use(function(req, res, next) {
  res.header(
    "Access-Control-Allow-Headers",
    "x-access-token, Origin, Content-Type, Accept"
  );
  next();
});

// app.get("/api/users", authJwt.verifyToken, (req, res) => {
  
//   res.json({ message: "User data retrieved successfully" });
// })

const boardroute = require("./app/routes/board.route");
app.use("/api/users",authJwt.verifyToken, boardroute);

require("./app/routes/auth.route")(app);


app.get("/", (req, res) => {
  res.json({ message: "Welcome to the Trello application." });
});

async function startServer() {
  try {


    await db.sequelize.sync();
    console.log("Synced db.");

    // await db.sequelize.sync({ force: true });
    // console.log("Drop and Resync Db");
    // initial()

    app.listen(PORT, () => {
      console.log(`Server is running on port ${PORT}.`);
    });
  } catch (err) {
    console.log("Failed to start server: " + err.message);
  }
}


function initial() {
  const Tag = db.Tag;
  const tags = [
    { tagId: 1, tagName: "red" },
    { tagId: 2, tagName: "green" },
    { tagId: 3, tagName: "yellow" },
    { tagId: 4, tagName: "blue" },
    { tagId: 5, tagName: "black" }
  ];

  tags.forEach(tag => {
    Tag.create(tag);
  });
}

startServer();
